#ifndef STRINGS_H
#define STRINGS_H

#include<stdio.h>
#include<stdlib.h>

// try to get rid of:
#include<ctype.h>

typedef char Character;
typedef struct String_character String_character;
typedef String_character* String;

struct String_character{
	String_character* next;
	Character character;
};

typedef struct {
	String (*new)(char*);
	void (*delete)(String);

	String (*add_char)(String, char);
	Character (*character_at)(String, int);

	int (*compare)(String, String);
	int (*equals)(String, String);
	int (*unequals)(String, String);

	String (*append)(String, String);
	String (*substring)(String, int, int);
	int (*contains)(String, String);
	String (*replace)(String, String, String);

	String (*lowercase)(String);
	String (*uppercase)(String);

	int (*length)(String);
	char* (*to_string)(String);
	void (*print)(String);
	void (*debug)(String);
} Strings_structure;

extern Strings_structure Strings;

#endif
