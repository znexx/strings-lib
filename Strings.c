#include<Strings.h>
static String New_string(char*);
static void Delete_string(String);

static String Append_character(String, Character);
static Character Get_character(String string, int index);

static String Append_string(String, String);
static String Sub_string(String, int, int);

static int String_length(String);
static char* To_string(String);
static void Print_string(String);

#define print_param(x,y) printf(#x": "#y"\n", x)

static String New_string(char* old_string){
	String string = NULL;
	int i = 0;
	while(old_string[i] != '\0'){
		string = Append_character(string, old_string[i]);
		i++;
	}
	return string;
}

static void Delete_string(String string){
	String_character* current = string;
	String_character* next;
	while(current){
		next = current->next;
		current->character = '\0';
		current->next = NULL;
		free(current);
		current = next;
	}
}

static String Append_character(String string, Character character){
	String_character* new = malloc(sizeof(String_character));
	String_character* last = string;
	if(string == NULL){
		string = new;
	}else{
		while(last && last->next){
			last = last->next;
		}
		last->next = new;
	}
	new->character = character;
	new->next = NULL;
	return string;
}

static Character Get_character(String string, int index){
	String_character* current = string;
	while(current){
		if(index == 0){
			return current->character;
		}
		index--;
		current = current->next;
	}
	return '\0';
}

static String Append_string(String string1, String string2){
	String string3;
	int length1 = String_length(string1);
	int length2 = String_length(string2);
	int length3 = length1 + length2;
	int i;
	char* concat = malloc((length3+1)*sizeof(char));
	char* tmp;

	tmp = To_string(string1);
	for(i=0; i<length1; i++){
		concat[i] = tmp[i];
	}
	free(tmp);

	tmp = To_string(string2);
	for(i=0; i<length2; i++){
		concat[length1 + i] = tmp[i];
	}
	free(tmp);

	concat[length3] = '\0';
	string3 = New_string(concat);
	free(concat);
	return string3;
}

static String Sub_string(String string, int start, int length){
	String out = NULL;
	char* orig_chars = To_string(string);
	char* chars = &orig_chars[start];
	orig_chars[start+length] = '\0';
	out = New_string(chars);
	free(orig_chars);
	return out;
}

static int String_length(String string){
	int length = 0;
	String_character* current = string;
	while(current){
		length++;
		current = current->next;
	}
	return length;
}

static char* To_string(String string){
	char* char_string = malloc((String_length(string)+1)*sizeof(char));
	int i = 0;
	String_character* current = string;
	while(current){
		char_string[i] = current->character;
		i++;
		current = current->next;
	}
	char_string[i] = '\0';
	return char_string;
}

static void Print_string(String string){
	String_character* current = string;
	while(current){
		putc(current->character, stdout);
		current = current->next;
	}
	putc('\n', stdout);
}

static void Debug_string(String string){
	String_character* current = string;
	int i = 0;
	printf("String %p (length %d):\n", (void*)string, String_length(string));
	while(current){
		printf("%d: >%c< (%d)\n", i++, current->character, current->character);
		current = current->next;
	}
}

static int Compare(String string1, String string2){
	String_character* current1 = string1;
	String_character* current2 = string2;
	while(current1 && current2){
		if(current1->character < current2->character){
			return -1;
		}else if(current1->character > current2->character){
			return 1;
		}
		current1 = current1->next;
		current2 = current2->next;
	}
	if(current2){
		return -1;
	}else if(current1){
		return 1;
	}else{
		return 0;
	}
}

static int Equals(String string1, String string2){
	String_character* current1 = string1;
	String_character* current2 = string2;
	while(current1 && current2){
		if(current1->character != current2->character){
			return 0;
		}
		current1 = current1->next;
		current2 = current2->next;
	}
	return 1;
}

static int Unequals(String string1, String string2){
	return !Equals(string1, string2);
}

static int Contains(String string1, String string2){
	String_character* current1 = string1;
	String_character* current1tmp;
	String_character* current2;
	int index = 0;
	while(current1){
		current2 = string2;
		current1tmp = current1;
		while(current1 && current2 && current1->character == current2->character){
			if(current2->next == NULL){ // the last character of string2 matched
				return index;
			}
			current1 = current1->next;
			current2 = current2->next;
		}
		index++;
		current1 = current1tmp;
		current1 = current1->next;
	}
	return -1;
}

static String Lowercase(String string){
	char *tmp = To_string(string);
	int i = 0;
	while(tmp[i] != '\0'){
		tmp[i] = tolower(tmp[i]);
		i++;
	}
	String lower = New_string(tmp);
	free(tmp);
	return lower;
}

static String Uppercase(String string){
	char *tmp = To_string(string);
	int i = 0;
	while(tmp[i] != '\0'){
		tmp[i] = toupper(tmp[i]);
		i++;
	}
	String upper = New_string(tmp);
	free(tmp);
	return upper;
}

static String Replace(String string1, String string2, String string3){
	string2 = string2;
	string3 = string3;
	char *tmp = To_string(string1);
	String string = New_string(tmp);
	free(tmp);
	return string;
}

// compare in: two strings out: (-1, 0, 1)
// equals in: two strings out: boolean
// unequals in: two strings out: boolean
// contains (substring) in: two strings out: index of substring in first string
// lowercase
// uppercase
// replace

Strings_structure Strings = {
	.new = &New_string,
	.delete = &Delete_string,

	.add_char = &Append_character,
	.character_at = &Get_character,

	.compare = &Compare,
	.equals = &Equals,
	.unequals = &Unequals,

	.append = &Append_string,
	.substring = &Sub_string,
	.contains = &Contains,
	.replace = &Replace,

	.lowercase = &Lowercase,
	.uppercase = &Uppercase,

	.length = &String_length,
	.to_string = &To_string,
	.print = &Print_string,
	.debug = &Debug_string
};
