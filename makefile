SHELL=/bin/sh
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Wno-unused-function -Og -g -I .
LDFLAGS=-L/usr/local/lib -pedantic -Wall -Wextra
SRC=$(wildcard *.c)

$(notdir $(shell pwd)): $(SRC)
	gcc -o $@ $^ $(CFLAGS) $(LDFLAGS)
	
test:
	./$(notdir $(shell pwd))

clean:
	rm -rf *.o $(notdir $(shell pwd))
