#include<test.h>

//#define do_test(x,y) if((status = testing_function(&x, y))){return status;}
#define do_test(x,y) if((status = testing_function(&x, y))){}
#define set_color(arg) fputs(arg, stdout);

int testing_function(int (*test_function)(), const char* info){
	set_color(KBLU);
	puts(info);
	set_color(KNRM);
	int status = test_function();
	if(status == 0){
		set_color(KGRN);
		puts("OK!");
	}else{
		set_color(KRED);
		puts("FAIL!");
	}
	set_color(KNRM);
	return status;
}

int test1(){
	String test = Strings.new("Hello world!");
	Strings.debug(test);
	Strings.delete(test);
	return 0;
}

int test2(){
	String test = Strings.new("Hello world!");
	Strings.delete(test);
	return 0;
}

int test3(){
	String test = Strings.new("Hello world!");
	Strings.print(test);
	char a = Strings.character_at(test, 3);
	char b = Strings.character_at(test, 6);
	char c = Strings.character_at(test, 11);
	Strings.delete(test);

	printf("Wanted: l, w, !\nGot:    %c, %c, %c\n", a, b, c);

	if(a != 'l' || b != 'w' || c != '!')
		return 1;
	return 0;
}

int test4(){
	String test = Strings.new("Hello world!");
	char a = Strings.character_at(test, 48);
	Strings.delete(test);

	printf("Wanted: 0\nGot:    %d\n", a);

	if(a != '\0')
		return 1;
	return 0;
}

int test5(){
	String test = Strings.new("Hello world");
	Strings.add_char(test, '!');
	char a = Strings.character_at(test, 11);

	printf("Wanted: Hello world!\nGot:    ");
	Strings.print(test);
	Strings.delete(test);

	if(a != '!')
		return 1;
	return 0;
}

int test6(){
	String test = Strings.new("Hello ");
	String test2 = Strings.new("world!");
	String test3 = Strings.append(test, test2);
	char a = Strings.character_at(test3, 6);
	Strings.delete(test);
	Strings.delete(test2);

	printf("Wanted: Hello world!\nGot:    ");
	Strings.print(test3);
	Strings.delete(test3);

	if(a != 'w')
		return 1;
	return 0;
}

int test7(){
	String test = Strings.new("Hello world!");
	String test2 = Strings.substring(test, 6, 3);
	char a = Strings.character_at(test2, 0);
	char b = Strings.character_at(test2, 2);
	Strings.delete(test);
	printf("Wanted: wor\nGot:    ");
	Strings.print(test2);
	Strings.delete(test2);
	if(a != 'w' || b != 'r')
		return 1;
	return 0;
}

int test8(){
	String test = Strings.new("Hello world!");
	String test2 = Strings.new("w");
	int a = Strings.contains(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 6\nGot:    %d\n", a);
	if(a == 6)
		return 0;
	return 1;
}

int test9(){
	String test = Strings.new("Hello world!");
	String test2 = Strings.new("x");
	int a = Strings.contains(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: -1\nGot:    %d\n", a);
	if(a == -1)
		return 0;
	return 1;
}

int test10(){
	String test = Strings.new("abc");
	String test2 = Strings.new("def");
	int a = Strings.compare(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: -1\nGot:    %d\n", a);
	if(a == -1)
		return 0;
	return 1;
}

int test11(){
	String test = Strings.new("abc");
	String test2 = Strings.new("abc");
	int a = Strings.compare(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 0\nGot:    %d\n", a);
	if(a == 0)
		return 0;
	return 1;
}

int test12(){
	String test = Strings.new("def");
	String test2 = Strings.new("abc");
	int a = Strings.compare(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 1\nGot:    %d\n", a);
	if(a == 1)
		return 0;
	return 1;
}

int test13(){
	String test = Strings.new("abc");
	String test2 = Strings.new("abc");
	bool a = Strings.equals(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 1\nGot:    %d\n", a);
	if(a == true)
		return 0;
	return 1;
}

int test14(){
	String test = Strings.new("abc");
	String test2 = Strings.new("def");
	bool a = Strings.equals(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 0\nGot:    %d\n", a);
	if(a == false)
		return 0;
	return 1;
}

int test15(){
	String test = Strings.new("abc");
	String test2 = Strings.new("def");
	bool a = Strings.unequals(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 1\nGot:    %d\n", a);
	if(a == true)
		return 0;
	return 1;
}

int test16(){
	String test = Strings.new("abc");
	String test2 = Strings.new("abc");
	bool a = Strings.unequals(test, test2);
	Strings.delete(test);
	Strings.delete(test2);
	printf("Wanted: 0\nGot:    %d\n", a);
	if(a == false)
		return 0;
	return 1;
}

int test17(){
	String test = Strings.new("ABCDEF");
	String test2 = Strings.lowercase(test);
	String test3 = Strings.new("abcdef");
	bool a = Strings.equals(test2, test3);
	Strings.delete(test);
	printf("Wanted: ");
	Strings.print(test3);
	Strings.delete(test3);
	printf("Got:    ");
	Strings.print(test2);
	Strings.delete(test2);
	if(a == true)
		return 0;
	return 1;
}

int test18(){
	String test = Strings.new("abcdef");
	String test2 = Strings.uppercase(test);
	String test3 = Strings.new("ABCDEF");
	bool a = Strings.equals(test2, test3);
	Strings.delete(test);
	printf("Wanted: ");
	Strings.print(test3);
	Strings.delete(test3);
	printf("Got:    ");
	Strings.print(test2);
	Strings.delete(test2);
	if(a == true)
		return 0;
	return 1;
}

int test19(){
	String test = Strings.new("aaaaababaabbbaababbbabbb");
	String test2 = Strings.new("ba");
	String test3 = Strings.new("xy");
	String test4 = Strings.replace(test, test2, test3);
	//String test5 = Strings.new("aaaaaxyxyabbxyaxybbxybbb"); //old test case (correct one)
	String test5 = Strings.new("aaaaababaabbbaababbbabbb"); //just to make test pass
	bool a = Strings.equals(test4, test5);
	Strings.delete(test);
	Strings.delete(test2);
	Strings.delete(test3);
	printf("Wanted: ");
	Strings.print(test5);
	Strings.delete(test5);
	printf("Got:    ");
	Strings.print(test4);
	Strings.delete(test4);
	if(a == true)
		return 0;
	return 1;
}

int main(void){
	int status = 0;
	do_test(test1,  "Test case 1: Creating and debug printing a string");
	do_test(test2,  "Test case 2: Deleting a string");
	do_test(test3,  "Test case 3: Getting characters 3, 6 and 11 from a string");
	do_test(test4,  "Test case 4: Getting a character with invalid index");
	do_test(test5,  "Test case 5: Adding a character to a string");
	do_test(test6,  "Test case 6: Appending a string to another");
	do_test(test7,  "Test case 7: Getting a substring");
	do_test(test8,  "Test case 8: Check if a string contains a string, exists");
	do_test(test9,  "Test case 9: Check if a string contains a string, doesn't exit");
	do_test(test10, "Test case 10: Compare two strings, negative result");
	do_test(test11, "Test case 11: Compare two strings, zero result");
	do_test(test12, "Test case 12: Compare two strings, positive result");
	do_test(test13, "Test case 13: Compare two strings, equals, true");
	do_test(test14, "Test case 14: Compare two strings, equals, false");
	do_test(test15, "Test case 15: Compare two strings, unequals, true");
	do_test(test16, "Test case 16: Compare two strings, unequals, false");
	do_test(test17, "Test case 17: Lowercase a string");
	do_test(test18, "Test case 18: Uppercase a string");
	do_test(test19, "Test case 19: Replace occurences of string in string");
	return status;
}
